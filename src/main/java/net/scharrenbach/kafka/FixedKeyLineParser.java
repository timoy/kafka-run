package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import kafka.serializer.Decoder;

/**
 * <p>
 * This parser returns key-value pairs with a fixed key and the line as the
 * value.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class FixedKeyLineParser<K, V> extends AbstractLineParser<K, V> {

	private final byte[] _key;

	/**
	 * <p>
	 * Simply calls super
	 * {@link AbstractLineParser#AbstractLineParser(Decoder, Decoder)}.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * @param keyDecoder
	 * @param valueDecoder
	 * @param key
	 *            fixed key.
	 */
	public FixedKeyLineParser(Decoder<K> keyDecoder, Decoder<V> valueDecoder,
			Object key) {
		super(keyDecoder, valueDecoder);
		_key = key == null ? null : key.toString().getBytes();
	}

	/**
	 * <p>
	 * Simply calls super
	 * {@link FixedKeyLineParser#FixedKeyLineParser(Decoder, Decoder, String)}
	 * with a null value for the fixed key.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * @param keyDecoder
	 * @param valueDecoder
	 */
	public FixedKeyLineParser(Decoder<K> keyDecoder, Decoder<V> valueDecoder) {
		this(keyDecoder, valueDecoder, null);
	}

	/**
	 * <p>
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	@Override
	public KeyValuePair<byte[], byte[]> parseLine(String line) {
		return new KeyValuePair<byte[], byte[]>(_key, line.getBytes());
	}

}
