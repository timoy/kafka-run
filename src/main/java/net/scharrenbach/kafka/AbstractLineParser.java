package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import kafka.serializer.Decoder;

/**
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 * @param <K>
 * @param <V>
 */
public abstract class AbstractLineParser<K, V> implements LineParser<K, V> {

	private Decoder<K> _keyDecoder;

	private Decoder<V> _valueDecoder;

	/**
	 * <p>
	 * 
	 * </p>
	 * 
	 * @param keyDecoder
	 * @param valueDecoder
	 */
	public AbstractLineParser(Decoder<K> keyDecoder, Decoder<V> valueDecoder) {
		if (keyDecoder == null) {
			throw new NullPointerException();
		}
		if (valueDecoder == null) {
			throw new NullPointerException();
		}
		_keyDecoder = keyDecoder;
		_valueDecoder = valueDecoder;
	}

	//
	//
	//

	@Override
	public KeyValuePair<K, V> parse(String line) {
		KeyValuePair<byte[], byte[]> kvp = parseLine(line);

		return new KeyValuePair<K, V>(_keyDecoder.fromBytes(kvp.key),
				_valueDecoder.fromBytes(kvp.value));
	}

	/**
	 * <p>
	 * To be implemented by the client.
	 * </p>
	 * 
	 * @param line
	 * @return
	 */
	protected abstract KeyValuePair<byte[], byte[]> parseLine(String line);

}
