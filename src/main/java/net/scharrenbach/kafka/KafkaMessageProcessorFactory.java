package net.scharrenbach.kafka;

/**
 * <p>
 * Factory pattern for creating {@link KafkaMessageProcessor} objects.
 * </p>
 * <p>
 * This is needed for creating a {@link KafkaMessageProcessor} inside the
 * {@link KafkaConsumerRunnable#run()} method.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 * @param <K>
 *            type for key
 * @param <V>
 *            type for value
 */
public interface KafkaMessageProcessorFactory<K, V> {

	/**
	 * 
	 * @return
	 */
	KafkaMessageProcessor<K, V> createKafkaMessageProcessor();

}
