package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Properties;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple producer for Kakfa that writes messages to stdout.
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class KafkaProducer<K, V> {

	@SuppressWarnings("unused")
	private static final Logger _log = LoggerFactory
			.getLogger(KafkaProducer.class);

	//
	//
	//

	private Producer<K, V> _producer;

	protected boolean _started;

	protected Properties _properties;

	protected String _topic;

	//
	//
	//

	/**
	 * <p>
	 * Prepares a Kafka producer by creating a properties object and setting
	 * default values.
	 * </p>
	 * 
	 * <p>
	 * Note: the actual Kakfa producer is only being created in the
	 * {@link #run()} method.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public KafkaProducer() {
		_started = false;
		_properties = KafkaProperties.createProducerDefaultProperties();
		_topic = KafkaProperties.DEFAULT_TOPIC;
	}

	/**
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public void init() {
		// First of all set the started and the interrupted flag.
		setStarted(true);

		// Create a new producer from a new producer config.
		ProducerConfig config = new ProducerConfig(_properties);
		_producer = new Producer<K, V>(config);

	}

	/**
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public void shutdown() {
		// Close the producer.
		_producer.close();

		// Finally set the started flag to false.
		setStarted(false);
	}

	/**
	 * Creates a fresh {@link KeyedMessage} and sends it to the {@link Producer}
	 * .
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * @param key
	 * @param value
	 */
	public void write(K key, V value) {
		if (_started) {
			final KeyedMessage<K, V> message = new KeyedMessage<K, V>(_topic,
					key, value);			
			_producer.send(message);
		} else {
			final String errorMessage = "This producer has not yet been started!";
			throw new IllegalStateException(errorMessage);
		}
	}

	/**
	 * <p>
	 * Calls {@link #write(Object, Object)}.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * @param key
	 * @param value
	 */
	public void write(KeyValuePair<K, V> keyValuePair) {
		write(keyValuePair.key, keyValuePair.value);
	}

	//
	// Getters and setters.
	//

	/**
	 * <p>
	 * Note: this class references the specified properties object. Setting
	 * properties overwrites the reference directly. Please use a copy of your
	 * properties object in order not exclude unwanted side-effects.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * @param properties
	 */
	public void setProperties(Properties properties) {
		if (properties == null) {
			throw new NullPointerException();
		}
		_properties = properties;
	}

	/**
	 * <p>
	 * Note: this class references a properties object. Any changes to the
	 * original object will be reflected to the reference this class holds.
	 * Please use a copy of your properties object in order not exclude unwanted
	 * side-effects.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * 
	 * 
	 * @return
	 */
	public Properties getProperties() {
		return _properties;
	}

	/**
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * @param started
	 */
	private void setStarted(boolean started) {
		_started = started;
	}

	/**
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * @return
	 */
	public boolean isStarted() {
		return _started;
	}

	/**
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * @return
	 */
	public void setTopic(String topic) {
		_topic = topic;
	}

	/**
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 * @return
	 */
	public String getTopic() {
		return _topic;
	}

}
