package net.scharrenbach.kafka;

import kafka.message.MessageAndMetadata;

/**
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 * @param <K>
 * @param <V>
 */
public interface KafkaMessageProcessor<K, V> {

	/**
	 * Process a single Kafka message.
	 * 
	 * @param item
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	void processKafkaMessage(MessageAndMetadata<K, V> item);

}
